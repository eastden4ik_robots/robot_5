package ru.sviridoff.robots.robot_5;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.Message;

import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;
import ru.sviridoff.robots.robot_5.VK.Messenger;
import ru.sviridoff.robots.robot_5.VK.VkCore;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Robot_5 {

    private static Logger logger = new Logger();
    public static VkCore vkCore;
    public static Props props;
    static {
        try {
            vkCore = new VkCore();
            props = new Props("props.properties");
        } catch (ApiException | ClientException | IOException e) {
            logger.Logging(Logger.LogLevel.DANGER, e.getMessage());
        }
    }


    public static void main(String[] args) throws InterruptedException, ApiException {
        logger.Logging(Logger.LogLevel.INFO, "Robot 5 (VK_bot) - is running ...");
        Stash.props = props;
        while (true) {
            Thread.sleep(300);
            try {
                Message message = vkCore.getMessage();
                if (message != null) {
                    ExecutorService exec = Executors.newCachedThreadPool();
                    exec.execute(new Messenger(message));
                }

            } catch (ClientException e) {
                logger.Logging(Logger.LogLevel.DANGER, "Возникли проблемы");
                final int RECONNECT_TIME = 10000;
                logger.Logging(Logger.LogLevel.DANGER, "Повторное соединение через " + RECONNECT_TIME / 1000 + " секунд");
                Thread.sleep(RECONNECT_TIME);

            }
        }
    }


}
