package ru.sviridoff.robots.robot_5;

import com.google.gson.Gson;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.robots.robot_5.OpenWeatherModelAPI.WeatherModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetInfoAboutCity {

    public WeatherModel getInfo(String city) {
        String weather_token = Stash.props.getProperty("WEATHER.TOKEN");
        String url = String.format("https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s", city, weather_token);
        String result = "";
        try {
            URL obj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

            connection.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());
            result = response.toString();
        } catch (IOException ex) {
            Stash.logger.Logging(Logger.LogLevel.DANGER, ex.getMessage());
        }

        Gson gson = new Gson();
        return gson.fromJson(result, WeatherModel.class);
    }

}
