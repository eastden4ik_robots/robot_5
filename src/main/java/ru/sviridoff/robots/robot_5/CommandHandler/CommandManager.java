package ru.sviridoff.robots.robot_5.CommandHandler;

import ru.sviridoff.robots.robot_5.Commands.HelpCommand;
import ru.sviridoff.robots.robot_5.Commands.UnknownCommand;
import ru.sviridoff.robots.robot_5.Commands.WeatherCommand;

import java.util.HashSet;

public class CommandManager {
    private static HashSet<Command> commands = new HashSet<>();

    static {
        commands.add(new UnknownCommand("unknown"));
        commands.add(new WeatherCommand("weather"));
        commands.add(new WeatherCommand("погода"));
        commands.add(new HelpCommand("help"));
        commands.add(new HelpCommand("помощь"));
        commands.add(new HelpCommand("начать"));
        commands.add(new HelpCommand("start"));
    }

    public static HashSet<Command> getCommands(){
        return commands;
    }
    public static void addCommand(Command command) { commands.add(command);}
}