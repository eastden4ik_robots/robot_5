package ru.sviridoff.robots.robot_5.CommandHandler;

import com.vk.api.sdk.objects.messages.Message;
import ru.sviridoff.robots.robot_5.Commands.UnknownCommand;

import java.util.Collection;

public class CommandDeterminant {

    public static Command getCommand(Collection<Command> commands, Message message) {
        String body = message.getText().toLowerCase();
        for (Command command : commands
        ) {
            if (command.name.equals(body.split(" ")[0])) {
                return command;
            }
        }
        return new UnknownCommand("unknown");
    }
}