package ru.sviridoff.robots.robot_5.Commands;

import com.vk.api.sdk.objects.messages.Message;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.robots.robot_5.CommandHandler.Command;
import ru.sviridoff.robots.robot_5.Stash;
import ru.sviridoff.robots.robot_5.VK.VkManager;
import ru.sviridoff.robots.robot_5.OpenWeatherModelAPI.WeatherModel;

import java.text.DecimalFormat;

public class WeatherCommand extends Command {

    public WeatherCommand(String name) {
        super(name);
    }

    @Override
    public void exec(Message message) {
        String[] messageList = message.getText().split(" ");
        WeatherModel weatherModel = Stash.infoAboutCity.getInfo(messageList[messageList.length - 1]);
        StringBuilder result = new StringBuilder();
        String pattern = "##0.00";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        result.append("Погода в городе ").append(message.getText().split(" ")[1]).append(", ").append(":\n");
        result.append("Температура: ").append(decimalFormat.format(weatherModel.getMain().getTemp() - 273.15)).append(" \n");
        result.append("Минимальная температура: ").append(decimalFormat.format(weatherModel.getMain().getTemp_min() - 273.15)).append(" \n");
        result.append("Максимальная температура: ").append(decimalFormat.format(weatherModel.getMain().getTemp_max() - 273.15)).append(" \n");
        result.append("Влажность: ").append(weatherModel.getMain().getHumidity()).append(" \n");
        result.append("Скорость ветра: ").append(weatherModel.getWind().getSpeed()).append(" \n");
        new VkManager().sendMessage(result.toString(), message.getFromId());
        Stash.logger.Logging(Logger.LogLevel.INFO, result.toString());
        Stash.logger.Logging(Logger.LogLevel.INFO, "Message [Погода в городе] to " + message.getFromId());
    }
}
