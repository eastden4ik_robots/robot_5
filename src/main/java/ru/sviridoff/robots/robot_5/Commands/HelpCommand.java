package ru.sviridoff.robots.robot_5.Commands;

import com.vk.api.sdk.objects.messages.Message;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.robots.robot_5.CommandHandler.Command;
import ru.sviridoff.robots.robot_5.Stash;
import ru.sviridoff.robots.robot_5.VK.VkManager;

public class HelpCommand extends Command {

    public HelpCommand(String name) {
        super(name);
    }

    @Override
    public void exec(Message message) {
        StringBuilder result = new StringBuilder();
        result.append("Бот для проверки погоды в указанном городе: \n");
        result.append("Для того, чтобы узнать погоду введите\nодну из команд ниже.\nПримеры:\n");
        result.append("1. погода {название_города}\n");
        result.append("2. погода в {название_города}\n");
        result.append("3. weather {название_города}\n");
        result.append("4. weather in {название_города}\n");
        result.append("P.S. Знаки фигурных скобок писать не нужно и регистр комманд может быть любой.\n");
        new VkManager().sendMessage(result.toString(), message.getFromId());
        Stash.logger.Logging(Logger.LogLevel.INFO, "Message [Help command] to " + message.getFromId());
    }

}
