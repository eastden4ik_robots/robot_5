package ru.sviridoff.robots.robot_5.Commands;

import com.vk.api.sdk.objects.messages.Message;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.robots.robot_5.CommandHandler.Command;
import ru.sviridoff.robots.robot_5.Stash;
import ru.sviridoff.robots.robot_5.VK.VkManager;

public class UnknownCommand extends Command {

    public UnknownCommand(String name) {
        super(name);
    }

    @Override
    public void exec(Message message) {

        new VkManager().sendMessage("Извините, но такой команды нет.\nВведите команду [help] или [помощь], чтобы узнать доступные команды", message.getFromId());
        Stash.logger.Logging(Logger.LogLevel.INFO, "Message [Извините, но такой команды нет.\nВведите команду [help] или [помощь], чтобы узнать доступные команды] to " + message.getFromId());
    }
}