package ru.sviridoff.robots.robot_5.VK;

import com.vk.api.sdk.objects.messages.Message;
import ru.sviridoff.robots.robot_5.CommandHandler.Commander;

public class Messenger implements Runnable{

    private Message message;

    public Messenger(Message message){
        this.message = message;
    }

    @Override
    public void run() {
        Commander.execute(message);
    }
}