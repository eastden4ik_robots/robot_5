package ru.sviridoff.robots.robot_5.VK;


import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;

import com.vk.api.sdk.objects.messages.Message;
import com.vk.api.sdk.queries.messages.MessagesGetLongPollHistoryQuery;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;

import java.io.IOException;
import java.util.List;

public class VkCore {

    private VkApiClient vk;
    private static int ts;
    private GroupActor actor;
    private static int maxMsgId = -1;
    private static Logger logger = new Logger();

    private static Props props;

    static {
        try {
            props = new Props("props.properties");
        } catch (IOException ex) {
            logger.Logging(Logger.LogLevel.DANGER, ex.getMessage());
        }
    }

    public VkCore() throws ClientException, ApiException {
        TransportClient transportClient = HttpTransportClient.getInstance();
        vk = new VkApiClient(transportClient);

        int groupId;
        String access_token;
        groupId = Integer.parseInt(props.getProperty("VK.GROUP.ID"));
        access_token = props.getProperty("VK.TOKEN");
        actor = new GroupActor(groupId, access_token);
        ts = vk.messages().getLongPollServer(actor).execute().getTs();
    }

    public Message getMessage() throws ClientException, ApiException {

        MessagesGetLongPollHistoryQuery eventsQuery = vk.messages()
                .getLongPollHistory(actor)
                .ts(ts);
        if (maxMsgId > 0){
            eventsQuery.maxMsgId(maxMsgId);
        }
        List<Message> messages = eventsQuery
                .execute()
                .getMessages()
                .getItems();

        if (!messages.isEmpty()){
            try {
                ts =  vk.messages()
                        .getLongPollServer(actor)
                        .execute()
                        .getTs();

            } catch (ClientException e) {
                e.printStackTrace();

            }

        }

        if (!messages.isEmpty() && !messages.get(0).isOut()) {
            /*
            messageId - максимально полученный ID, нужен, чтобы не было ошибки 10 internal server error,
            который является ограничением в API VK. В случае, если ts слишком старый (больше суток),
            а max_msg_id не передан, метод может вернуть ошибку 10 (Internal server error).
             */
            int messageId = messages.get(0).getId();
            if (messageId > maxMsgId){
                maxMsgId = messageId;
            }
            return messages.get(0);
        }
        return null;
    }

    public VkApiClient getVk() {
        return vk;
    }

    public void setVk(VkApiClient vk) {
        this.vk = vk;
    }

    public GroupActor getActor() {
        return actor;
    }

    public void setActor(GroupActor actor) {
        this.actor = actor;
    }
}
